var express = require('express');
var router = express.Router();
var Statistics = require('../model/StatsCalculations.js');

/* GET Stats. */
router.get('/', function(req, res, next) {
  	res.render('jasmine', { title: 'Jasmine Testing', stats: Statistics });
});

module.exports = router;