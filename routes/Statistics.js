var express = require('express');
var router = express.Router();
var Statistics = require('../model/StatsCalculations.js');

/* GET Stats. */
router.get('/*', function(req, res, next) {
	var values = JSON.parse(req.params[0]);
	console.log(values);
	var stats = new Statistics(values);
	var response = {
		Range: 0,
		Median: 0,
		Mean: 0,
		PSD: 0
	}
	
	if (!stats.isArray()) {
		
		response.Range = 'bad data';
		response.Median = 'bad data';
		response.Mean = 'bad data';
		response.PSD = 'bad data';
		
		var jsonResponse = JSON.stringify(response);
		res.send(jsonResponse);
	} else {
		response.Range = stats.Range();
		response.Median = stats.Median();
		response.Mean = stats.Mean();
		response.PSD = stats.PopulationStandardDeviation(stats.Mean());
		
		var jsonResponse = JSON.stringify(response);
		res.send(jsonResponse);
	}
	
	
});

module.exports = router;