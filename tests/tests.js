﻿describe("When pass in numbers for calculations", function() {
    it("should return 0 for Range if array has only one number.", function() {
		var numbers = [1];
		var stats = new Statistics(numbers);
        expect(stats.Range()).toEqual(0);
    });
	
	it("should return same number for Median if array has only one number.", function() {
		var numbers = [1];
		var stats = new Statistics(numbers);
        expect(stats.Median()).toEqual(1);
    });
	
	it("should return same number for Mean if array has only one number.", function() {
		var numbers = [1];
		var stats = new Statistics(numbers);
        expect(stats.Mean()).toEqual(1);
    });
	
	it("should return 0 for Population Standard Deviation if array has only one number.", function() {
		var numbers = [1];
		var stats = new Statistics(numbers);
        expect(stats.PopulationStandardDeviation(stats.Mean())).toEqual(0);
    });
	
	it("should return 0 for Range if array has same two 1's.", function() {
		var numbers = [1, 1];
		var stats = new Statistics(numbers);
        expect(stats.Range()).toEqual(0);
    });
	
	it("should return 1 for Median if array has two 1's.", function() {
		var numbers = [1, 1];
		var stats = new Statistics(numbers);
        expect(stats.Median()).toEqual(1);
    });
	
	it("should return 1 for Mean if array has has two 1's.", function() {
		var numbers = [1, 1];
		var stats = new Statistics(numbers);
		console.log(stats.Mean());
        expect(stats.Mean()).toBeCloseTo(1, 0);
    });
	
	it("should return 0 for Population Standard Deviation if array has two 1's.", function() {
		var numbers = [1, 1];
		var stats = new Statistics(numbers);
        expect(stats.PopulationStandardDeviation(stats.Mean())).toBeCloseTo(0, 0);
    });
	
	it("should return 0 for Range if array has three numbers.", function() {
		var numbers = [1, 1, 2];
		var stats = new Statistics(numbers);
        expect(stats.Range()).toEqual(1);
    });
	
	it("should return 1 for Median if array has three numbers.", function() {
		var numbers = [1, 1, 2];
		var stats = new Statistics(numbers);
        expect(stats.Median()).toEqual(1);
    });
	
	it("should return 1 for Mean if array has has three numbers.", function() {
		var numbers = [1, 1, 2];
		var stats = new Statistics(numbers);
		console.log(stats.Mean());
        expect(stats.Mean()).toBeCloseTo(1.33, 0);
    });
	
	it("should return 0 for Population Standard Deviation if array has three numbers.", function() {
		var numbers = [1, 1, 2];
		var stats = new Statistics(numbers);
        expect(stats.PopulationStandardDeviation(stats.Mean())).toBeCloseTo(0, 0);
    });
	
		it("should return 14 for Range if array has three numbers.", function() {
		var numbers = [10, 12, 24];
		var stats = new Statistics(numbers);
        expect(stats.Range()).toEqual(14);
    });
	
	it("should return 12 for Median if array has three numbers.", function() {
		var numbers = [10, 12, 24];
		var stats = new Statistics(numbers);
        expect(stats.Median()).toEqual(12);
    });
	
	it("should return 15.33 for Mean if array has has three numbers.", function() {
		var numbers = [10, 12, 24];
		var stats = new Statistics(numbers);
		console.log(stats.Mean());
        expect(stats.Mean()).toBeCloseTo(15.33, 0);
    });
	
	it("should return 38.22 for Population Standard Deviation if array has three numbers.", function() {
		var numbers = [10, 12, 24];
		var stats = new Statistics(numbers);
        expect(stats.PopulationStandardDeviation(stats.Mean())).toBeCloseTo(38.22, 0);
    });
	
	it("should return 12.5 for Median if array has four numbers.", function() {
		var numbers = [10, 12, 13, 24];
		var stats = new Statistics(numbers);
        expect(stats.Median()).toBeCloseTo(12.5, 0);
    });
});