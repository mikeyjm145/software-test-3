var express = require('express');
var router = express.Router();

module.exports = function(listOfNumbers) {
	function checkListSize(returnValue) {
		if (listOfNumbers.length == 0) {
			return 0;
		}
		else if (listOfNumbers.length == 1) {
			return returnValue;
		} else {
			return null;
		}
	}
	
	this.Range = function() {
		var result = checkListSize(0);
		
		if (result != null) {
			return result;
		}
		
		result = 0;
		
		listOfNumbers.sort(function(a, b) {
			return a - b;
		});
		
		var biggestNumber = listOfNumbers[listOfNumbers.length - 1];
		var smallestNumber = listOfNumbers[0];
		
		result = biggestNumber - smallestNumber;
		return result;
	};
	
	this.Mean = function() {
		var result = checkListSize(listOfNumbers[0]);
		if (result != null) {
			return result;
		}
		
		result = 0;
		
		var sum = 0;
		listOfNumbers.forEach(function(number) {
			sum += number;
		});
		
		result = sum / listOfNumbers.length
		return result.toFixed(2);
	};
	
	this.Median = function() {
		var result = checkListSize(listOfNumbers[0]);
		if (result != null) {
			return result;
		}
		
		result = 0;
		
		listOfNumbers.sort(function(a, b) {
			return a-b;
		});
		
		var middle = listOfNumbers.length / 2;
		
		if (listOfNumbers.length % 2 === 0) {
			result = listOfNumbers[Math.ceil(middle) - 1] + listOfNumbers[Math.ceil(middle)];
			result = result / 2;
		} else {
			result = listOfNumbers[Math.ceil(middle) - 1];
		}
		
		return result;
	};
	
	this.PopulationStandardDeviation = function (mean) {
		if (mean == null || mean == undefined) {
			return "bad data.";
		}
	
		var result = checkListSize(0);
		if (result != null) {
			return result;
		}
	
		result = 0;
	
		var summation = 0;
		listOfNumbers.forEach(function (number) {
			summation += Math.pow(number - mean, 2);
		});
		
		result = summation / listOfNumbers.length;
		return result.toFixed(2);
	};
	
	this.isArray = function() {
		return Object.prototype.toString.call(listOfNumbers) === '[object Array]';
	};
};