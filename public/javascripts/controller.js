var statisticsEngine = function($scope, $resource, $http, $q, calculations, getStats, results) {
	$scope.numbers = '';
	$scope.range = calculations.range;
	$scope.median = calculations.median;
	$scope.mean = calculations.mean;
	$scope.PSD = calculations.PSD;
	$scope.aNumber = 0;
	$scope.noError = true;
	
	var getCalculations = function(theNumbers) {
		if (calculations === null
			|| calculations.numbers === null
			|| calculations.numbers.length === 0) {
			return;
		}
		
		var numberArrayJSON = JSON.stringify(theNumbers);
		var deferred = $q.defer();
		
		$http.get('/statistics/' + numberArrayJSON).
		success(function(data, status, headers, config) {
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			
		});
		
		return deferred.promise;
	};
	
	$scope.updateValues = function () {
		if ($scope.aNumber === undefined || $scope.aNumber === null || $scope.aNumber === '') {
			$scope.noError = false;
			return;
		}
		
		if (parseInt($scope.aNumber) === null || parseInt($scope.aNumber) === undefined) {
			$scope.noError = false;
			return;
		}
		
		$scope.noError = true;
		
		var value = parseInt($scope.aNumber);
		calculations.numbers.push(value);
		$scope.numbers += value;
		$scope.numbers += ', ';
		
		var serverResults = null;
		getCalculations(calculations.numbers)
		.then(function(response) {
			$scope.noError = true;
			
			calculations.range = response.Range;
			calculations.median = response.Median;
			calculations.mean = response.Mean;
			calculations.PSD = response.PSD;
			
			$scope.range = calculations.range;
			$scope.median = calculations.median;
			$scope.mean = calculations.mean;
			$scope.PSD = calculations.PSD;
			$scope.aNumber = 0;
		}).catch(function(data, status) {
			console.log(status + " " + data);
			$scope.noError = false;
		});
	};
	
	$scope.clearValues = function () {
		$scope.aNumber = 0;
		$scope.noError = true;
	};
	
	$scope.clearAllValues = function() {
		$scope.noError = true;
		
		calculations.numbers = [];
		calculations.range = 0;
		calculations.median = 0;
		calculations.mean = 0.00;
		calculations.PSD = 0.00;
		
		$scope.range = calculations.range;
		$scope.median = calculations.median;
		$scope.mean = calculations.mean;
		$scope.PSD = calculations.PSD;
		$scope.aNumber = 0;
		$scope.numbers = '';
	};
};

StatisticsEngine.controller('StatsEngine', [
	'$scope',
	'$resource',
	'$http',
	'$q',
	'Calculations',
	statisticsEngine
]);