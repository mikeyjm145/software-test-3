StatisticsEngine.factory('Calculations', function() {
	var calculations = {
		numbers: [],
		range: 0,
		median: 0,
		mean: 0.00,
		PSD: 0.00
	};
	
	return calculations;
});